# Onshape lasercut box 

How to desing a box for laser cutting.

1. Go to [Onshape](onshape.com) and sign in.

--------------------------------

2. Then go to this link:

https://cad.onshape.com/documents/578830e4e4b0e65410f9c34e/v/75269c4a1a5ece98be1e2b6f/e/dfd5effddfd7f2ecce4b0246

--------------------------------

3. Click as shown in the image to activate the feature:

![](laserbox_001.png)

--------------------------------

4. Create a new onshape document (call it "box_*yourname*").

--------------------------------

5. Hide **top**, **front** and **right** planes. Then go to the list of elements and click on the top plane and create a new sketch. Change the view to **top**(on the navigation cube).

![](laserbox_002.png)

--------------------------------

6. Use **center point rectangle** tool to create a rectangle starting in the origin point. Set the horizontal and vertical size to 70mm. Click on the green check.

![](laserbox_003.png)

--------------------------------

7. Select the sketch you just created and click on the **extrude** tool. Set the depth to 70mm and click the green check.

--------------------------------

8. Now, select the front and back faces of the cube and click on the **thicken** tool. On the **thicken window** select **New**, change **direction 1** to 3mm and click on the arrow next to **direction 1** (the arrow should point up and to the right).

![](laserbox_004.png)

--------------------------------

9. When you click the green check you should see something like the image below.

![](laserbox_005.png)

--------------------------------

10. Select the **right** and **left** faces and repeat the **thicken** process (**New**, **direction 1** to 3mm, and click the arrow.) (take a look at the image below to verify the process). Click the green check.

![](laserbox_006.png)

--------------------------------

11. Make sure the new panels are intersecting each other on the corners.

![](laserbox_007.png)

--------------------------------

12. Now, repeat the thicken process on the bottom face of the cube (do not apply to the top face).

![](laserbox_008.png)

--------------------------------

13. Rotate the view so you can see the top of the cube. Then click on the **delete part** tool.

![](laserbox_009.png)

--------------------------------

14. Click on the top face of the cube. The cube should disappear.

![](laserbox_010.png)

--------------------------------

15. Now, go to the right of the tool bar and click on **search tools**, then write "laser" and you should see the **Laser Joint** tool. Click on it.

![](laserbox_011.png)

--------------------------------

16. Now, click and drag so you enclose the entire design in the selection area. Relase the click when everything is selected.

![](laserbox_012.png)

--------------------------------

17. After a moment, you should see the box panels have tabs on the borders.

![](laserbox_013.png)

--------------------------------

18. Click the green check.

![](laserbox_014.png)

--------------------------------

19. To add the **Auto Layout** feature, go to this link: https://cad.onshape.com/documents/576e01dbe4b0cc2e7f46a55d/v/731e73ac0b7b1e4334f13106/e/887d6e2324589bfd2058c3e1

![](laserbox_015.png)

--------------------------------

20. In **search tools** look for **auto layout** and click on it.

![](laserbox_016.png)

--------------------------------

21. Change the **thickness of the material** to 3mm and click on the green check.

![](laserbox_017.png)

--------------------------------

22. Turn in a screenshot ot the entire screen.

![](laserbox_018.png)
